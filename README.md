### 机器学习
**LinearRegression** 线性回归

- SimpleLinearRegression 一个简单的线性回归模型，自己给出几个数据，进行了一下简单的预测。
- LinearRegression 使用sklearn库中的LinearRegression对数据进行预测。详细介绍可看我[简书](https://www.jianshu.com/p/a360f139b076)

 **LogisticRegression**  逻辑回归

- LogisticRegression 梯度下降算法拟合逻辑回归参数值。
- Titanic 使用sklearn库中的LogisticRegression对数据进行预测。数据来源，kaggle中Titanic的比赛。

 **Decision Tree**  决策树

- DecisionTree 使用决策树去预测自己给出的一些数据

 **Neural Network**  神经网络

- NeuralNetwork 神经网络算法
- NNImplementation 调用NeuralNetwork中的算法，查看Neural Network算法实现效果
- HandWritten 调用NeuralNetwork中的算法对sklearn库中的数据load_digits进行预测

 **CollaborativeFiltering**  协同过滤
- 简单的试了一下推荐系统，详细可看我的[简书](https://www.jianshu.com/p/20041e72e9ec)


 **Titanic Machine Learning from Disaster** 

- Titanic Kaggle上的一个比赛，详细可看我[简书](https://www.jianshu.com/p/935bc22157e1)

 **Digit Recognizer** 
- DigitRecognizer 手写数字识别

 **Naive Bayes** 

- Naive Bayes 朴素贝叶斯算法
- GaussianNB 使用高斯分布型朴素贝叶斯对sklearn库中的自带的数据iris进行预测。详细可看我[简书](https://www.jianshu.com/p/37174d682553)

**error BackPropagation**
- BP 西瓜书第5章误差逆传播算法，详细可看我[简书](https://www.jianshu.com/p/197ca901c9cf)
- watermelon_3 西瓜数据集3.0，在西瓜书P84
