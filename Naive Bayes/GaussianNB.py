"""
【IRIS数据集】

由Fisher在1936年整理，包含4个特征（Sepal.Length（花萼长度）、Sepal.Width（花萼宽度）、Petal.Length（花瓣长度）、
Petal.Width（花瓣宽度）），特征值都为正浮点数，单位为厘米。
目标值为鸢尾花的分类（Iris Setosa（山鸢尾）、Iris Versicolour（杂色鸢尾），Iris Virginica（维吉尼亚鸢尾））。
"""

from sklearn import datasets
from sklearn.model_selection import train_test_split

iris = datasets.load_iris()
print(iris.data)

x_train, x_test, y_train, y_test = train_test_split(iris.data, iris.target, test_size=0.3, random_state=0)


#我们假定sepal length, sepal width, petal length, petal width 4个量独立且服从高斯分布，用贝叶斯分类器建模
from sklearn.naive_bayes import GaussianNB
gnb = GaussianNB()
y_pred = gnb.fit(x_train, y_train).predict(x_test)
right_num = (y_test == y_pred).sum()
print("Total testing num :%d , naive bayes accuracy :%f" %(x_test.shape[0], float(right_num)/x_test.shape[0]))

